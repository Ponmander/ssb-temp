.section .text
glabel func_80009A84
  /* 00A684 80009A84 27BDFFE0 */     addiu $sp, $sp, -0x20
  /* 00A688 80009A88 AFB00018 */        sw $s0, 0x18($sp)
  /* 00A68C 80009A8C 00808025 */        or $s0, $a0, $zero
  /* 00A690 80009A90 10800004 */      beqz $a0, .L80009AA4
  /* 00A694 80009A94 AFBF001C */        sw $ra, 0x1c($sp)
  /* 00A698 80009A98 3C0E8004 */       lui $t6, %hi(D_80046A54)
  /* 00A69C 80009A9C 8DCE6A54 */        lw $t6, %lo(D_80046A54)($t6)
  /* 00A6A0 80009AA0 148E0004 */       bne $a0, $t6, .L80009AB4
  .L80009AA4:
  /* 00A6A4 80009AA4 240F0002 */     addiu $t7, $zero, 2
  /* 00A6A8 80009AA8 3C018004 */       lui $at, %hi(D_80046A64)
  /* 00A6AC 80009AAC 10000021 */         b .L80009B34
  /* 00A6B0 80009AB0 AC2F6A64 */        sw $t7, %lo(D_80046A64)($at)
  .L80009AB4:
  /* 00A6B4 80009AB4 0C002CE7 */       jal func_8000B39C
  /* 00A6B8 80009AB8 02002025 */        or $a0, $s0, $zero
  /* 00A6BC 80009ABC 9202000F */       lbu $v0, 0xf($s0)
  /* 00A6C0 80009AC0 24010001 */     addiu $at, $zero, 1
  /* 00A6C4 80009AC4 10410007 */       beq $v0, $at, .L80009AE4
  /* 00A6C8 80009AC8 24010002 */     addiu $at, $zero, 2
  /* 00A6CC 80009ACC 10410009 */       beq $v0, $at, .L80009AF4
  /* 00A6D0 80009AD0 24010003 */     addiu $at, $zero, 3
  /* 00A6D4 80009AD4 1041000B */       beq $v0, $at, .L80009B04
  /* 00A6D8 80009AD8 00000000 */       nop 
  /* 00A6DC 80009ADC 1000000C */         b .L80009B10
  /* 00A6E0 80009AE0 9218000D */       lbu $t8, 0xd($s0)
  .L80009AE4:
  /* 00A6E4 80009AE4 0C002DC3 */       jal func_8000B70C
  /* 00A6E8 80009AE8 02002025 */        or $a0, $s0, $zero
  /* 00A6EC 80009AEC 10000008 */         b .L80009B10
  /* 00A6F0 80009AF0 9218000D */       lbu $t8, 0xd($s0)
  .L80009AF4:
  /* 00A6F4 80009AF4 0C002DD8 */       jal func_8000B760
  /* 00A6F8 80009AF8 02002025 */        or $a0, $s0, $zero
  /* 00A6FC 80009AFC 10000004 */         b .L80009B10
  /* 00A700 80009B00 9218000D */       lbu $t8, 0xd($s0)
  .L80009B04:
  /* 00A704 80009B04 0C002604 */       jal func_80009810
  /* 00A708 80009B08 8E040074 */        lw $a0, 0x74($s0)
  /* 00A70C 80009B0C 9218000D */       lbu $t8, 0xd($s0)
  .L80009B10:
  /* 00A710 80009B10 24010041 */     addiu $at, $zero, 0x41
  /* 00A714 80009B14 13010003 */       beq $t8, $at, .L80009B24
  /* 00A718 80009B18 00000000 */       nop 
  /* 00A71C 80009B1C 0C001F3D */       jal func_80007CF4
  /* 00A720 80009B20 02002025 */        or $a0, $s0, $zero
  .L80009B24:
  /* 00A724 80009B24 0C001ECC */       jal func_80007B30
  /* 00A728 80009B28 02002025 */        or $a0, $s0, $zero
  /* 00A72C 80009B2C 0C001E6A */       jal func_800079A8
  /* 00A730 80009B30 02002025 */        or $a0, $s0, $zero
  .L80009B34:
  /* 00A734 80009B34 8FBF001C */        lw $ra, 0x1c($sp)
  /* 00A738 80009B38 8FB00018 */        lw $s0, 0x18($sp)
  /* 00A73C 80009B3C 27BD0020 */     addiu $sp, $sp, 0x20
  /* 00A740 80009B40 03E00008 */        jr $ra
  /* 00A744 80009B44 00000000 */       nop 

